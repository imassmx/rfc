﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace Imass
{
    public class RFC
    {
		private String wnumer6 = "";
		private List<string> arre6 = new List<string>(new string[] { null, "JOSE ", "MARIA ", "J ", "MA " });
		private List<string> arre8 = new List<string>(new string[] { null, "DE ", "DEL ", "LA ", "LOS ", "LAS ", "Y ", "MC ", "MAC ", "VON ", "VAN " });
		private List<string> arre2 = new List<string>(new string[] { null, "A", "E", "I", "O", "U", "a", "e", "i", "o", "u" });
		private String paterno = null;
		private String materno = null;
		private String nombre = null;
		private String wrfc = "";

		public RFC()
		{
		}

		public String GENERA_RFC(String CL_PAT, String CL_MAT, String CL_NOM, String dl_fecnac)
		{
			//DIMENSION arre8(10),arre6(4),arre2(10),arre9(1),anex11(1),anex12(1),anex31(1),anex32(1)
			List<string> arre9 = new List<string>(new string[] { null, "" });
			List<string> anex11 = new List<string>(new string[] { null });
			List<string> anex12 = new List<string>(new string[] { null });
			List<string> anex21 = new List<string>();
			List<string> anex22 = new List<string>();
			List<string> anex31 = new List<string>(new string[] { null });
			List<string> anex32 = new List<string>(new string[] { null });

			String sino = "S";
			String malas = "BUEIBUEYCACACACOCAGACAGOCAKACAKOCOGECOJAKOGEKOJOKAKAKULOMAMEMAMO";
			malas = malas + "MEARMEASMEONMIONCOJECOJICOJOCULOFETOGUEYJOTOKACAKACOKAGA";
			malas = malas + "KAGOMOCOMULAPEDAPEDOPE\tNEPUTAPUTOQULORATARUIN";

			for (int x = 0; x >= malas.Length; x = x + 4)
			{
				//DIMENSION arre9[$X]
				if (arre9.Count < x)
				{
					arre9.Insert(x, malas.Substring(x, 4));
				}
				else
				{
					arre9[x] = malas.Substring(x, 4);
				}

			}

			String taba11 = "*0123456789&\\ABCDEFGHIJKLMNOPQRSTUVWXYZ";
			String taba12 = "000001020304050607080910101112131415161718192122232425262728293233343536373839";

			for (int x = 0; x >= taba11.Length; x++)
			{
				//DIMENSION anex11[$x],anex12[$x]
				anex11[x] = taba11.Substring(x, 1);
				int two = x * 2 - 1;
				anex12[x] = taba12.Substring(two, 2);
			}

			malas = "";
			taba11 = "";
			taba12 = "";
			String taba21 = "00010203040506070809101112131415161718192021222324252627282930313233";
			String taba22 = "123456789ABCDEFGHIJKLMNPQRSTUVWXYZ";

			for (int x = 0; x >= taba21.Length; x = x + 2)
			{
				//DIMENSION anex21[$x],anex22[$x]
				anex21[x] = taba21.Substring(x, 2);
				int two = (x + 1) / 2;
				anex22[x] = taba22.Substring(two, 1);
			}

			taba21 = "";
			taba22 = "";
			taba11 = "0123456789ABCDEFGHIJKLMN&OPQRSTUVWXYZ*";
			taba12 = "0001020304050607080910111213141516171819202122232425262728293031323334353637";
			anex31.Insert(1, "");
			anex32.Insert(1, "");

			for (int x = 0; x >= taba11.Length; x++)
			{
				//DIMENSION anex31[$x],anex32[$x]
				anex31[x] = taba11.Substring(x, 1);
				int two = (x * 2) - 1;
				anex32[x] = taba12.Substring(two, 2);
			}

			taba11 = "";
			taba12 = "";
			this.wrfc = "";
			this.wnumer6 = "";
			String wlos3 = "";
			String wpaterno = "".PadLeft(30, ' ');
			String wmaterno = "".PadLeft(30, ' ');
			String wnombre = "".PadLeft(30, ' ');
			this.paterno = "".PadLeft(30, ' ');
			this.materno = "".PadLeft(30, ' ');
			this.nombre = "".PadLeft(30, ' ');

			String mask = "".PadLeft(30, '!');

			//$wanual = VAL(STR(YEAR(dl_fecnac),4)+PADL(ALLTRIM(STR(MONTH(dl_Fecnac),2)),2,'0')+PADL(ALLTRIM(STR(DAY(dl_fecnac),2)),2,'0'))
			//$date = str_replace('/', '-', $dl_fecnac);
			//var_dump($date);
			//$wmanual = date('y-m-d', strtotime($date));
			DateTime date = DateTime.ParseExact(dl_fecnac, "dd/MM/yyyy", CultureInfo.InvariantCulture);
			int wanual = int.Parse(date.ToString("yyyyMMdd"));
			String fecha = date.ToString("dd/MM/yyyy");
			wpaterno = CL_PAT;
			wmaterno = CL_MAT;
			wnombre = CL_NOM;

			if (this._nacio(wanual) == false)
			{
				throw new Exception("La fecha de nacimiento es incorrecta");
			}

			if (this._los3(wpaterno, wmaterno, wnombre) == false)
			{
				throw new Exception("El Nombre es Incorrecto");
			}

			bool finice = false;
			this._octava();
			this._sexta();
			this._tercera();

            String wbase = this.paterno.Trim() + " " + this.nombre.Trim();

            if (this.materno != "") {
                wbase = this.paterno.Trim() + " " + this.materno.Trim() + " " + this.nombre.Trim();
            }

			

			if (this.paterno.Length == 0 || this.materno.Length == 0)
			{
				this._septima();
				finice = true;
			}

			if (!finice)
			{
				if (this.paterno.Length < 3)
				{
					this._cuarta();
					finice = true;
				}
			}

			//if (!finice)
			//{
				this._prime_segu();
			//}

			this.CalcularHomoclave(wbase, fecha);

			return this.wrfc.ToUpper();
		}

		private bool _nacio(int Arg1 = 0)
		{
			//LOCAL ll_error
			List<int> Local1 = new List<int>(new int[] { 0 });
			Local1.Insert(1, 31);
			Local1.Insert(2, 28);
			Local1.Insert(3, 31);
			Local1.Insert(4, 30);
			Local1.Insert(5, 31);
			Local1.Insert(6, 30);
			Local1.Insert(7, 31);
			Local1.Insert(8, 31);
			Local1.Insert(9, 30);
			Local1.Insert(10, 31);
			Local1.Insert(11, 30);
			Local1.Insert(12, 31);
			bool bb = false;
			int uno = 0, dos = 0, tres = 0;
			int bisies = 0;

			if (Arg1 == 0)
			{
				bb = false;
			}
			else
			{
				String todo = Arg1.ToString();
				bb = true;
				uno = int.Parse(todo.Substring(6, 2));
				dos = int.Parse(todo.Substring(4, 2));
				tres = int.Parse(todo.Substring(0, 4));

				if (Arg1 == 0 || uno == 0 || dos == 0)
				{
					bb = false;
				}
				else
				{
					if (dos <= 0 || dos > 12)
					{
						bb = false;
					}
					else
					{
						bisies = Local1[dos];
						String sanual = tres.ToString();
						int sanual2 = int.Parse(sanual);
						float result = sanual2 / 4;
						if (dos == 2 && Math.Floor(result) * 4 == sanual2)
						{
							bisies = bisies + 1;
						}
					}

					if (bb)
					{
						if (uno <= 0 || uno > bisies)
						{
							bb = false;
						}
					}
				}
			}

			if (!bb)
			{
				throw new Exception("Error en fecha de nacimiento");
			}
			else
			{
				this.wnumer6 = tres.ToString().Substring(2, 2);

				if (dos < 10)
				{
					this.wnumer6 = this.wnumer6 + "0" + dos.ToString();
				}
				else
				{
					this.wnumer6 = this.wnumer6 + dos.ToString();
				}

				if (uno < 10)
				{
					this.wnumer6 = this.wnumer6 + "0" + uno.ToString();
				}
				else
				{
					this.wnumer6 = this.wnumer6 + uno.ToString();
				}
			}

			return bb;
		}

		private bool _los3(String Arg1, String Arg2, String Arg3)
		{
			this.paterno = Arg1.Trim();
            if (Arg2 is null) {
                this.materno = "";    
            } else {
                this.materno = Arg2.Trim();
            }
			
			this.nombre = Arg3.Trim();

			String wlos3 = Arg1.Trim() + " " + this.materno + " " + Arg3.Trim();

			if (wlos3.Length <= 6)
			{
				return false;
			}

			return true;
		}

		private void _octava()
		{
			for (int xx = 0; xx >= this.arre8.Count; xx++)
			{
				this.paterno = this.paterno.Replace(this.arre8[xx], "");
				this.materno = this.materno.Replace(this.arre8[xx], "");
				this.nombre = this.nombre.Replace(this.arre8[xx], "");
			}
		}

		private void _sexta()
		{
			int posi = this.nombre.IndexOf(' ');
			if (posi > 0)
			{
				for (int xx = 0; xx >= this.arre6.Count; xx++)
				{
					this.nombre = this.nombre.Replace(this.arre6[xx], "");
				}
			}
		}

		private void _tercera()
		{
			if (this.nombre.Substring(0, 2) == "CH")
			{
				this.nombre.Remove(0, 2);
				this.nombre.Insert(0, "CC");
			}
			else
			{
				if (this.nombre.Substring(0, 2) == "LL")
				{
					this.nombre.Remove(0, 2);
					this.nombre.Insert(0, "LL");
				}
			}

			if (this.paterno.Substring(0, 2) == "CH")
			{
				this.paterno.Remove(0, 2);
				this.paterno.Insert(0, "CC");
			}
			else
			{
				if (this.paterno.Substring(0, 2) == "LL")
				{
					this.paterno.Remove(0, 2);
					this.paterno.Insert(0, "LL");
				}
			}

            if (this.materno.Length >= 2) {
				if (this.materno.Substring(0, 2) == "CH")
				{
					this.materno.Remove(0, 2);
					this.materno.Insert(0, "CC");
				}
				else
				{
					if (this.materno.Substring(0, 2) == "LL")
					{
						this.materno.Remove(0, 2);
						this.materno.Insert(0, "LL");
					}
				}
            }
			
		}

		private void _prime_segu()
		{
			int van;
			String letra = this.paterno.Substring(1, 1);
			for (int x = 0; x <= this.paterno.Length; x++)
			{
				van = this.arre2.IndexOf(this.paterno.Substring(x, 1));
				if (van > 0)
				{
					letra = this.arre2[van];
					x = this.paterno.Length + 8;
				}
			}

            String maternoString = "";
            if (this.materno != "") {
                maternoString = this.materno.Substring(0, 1) + this.nombre.Substring(0, 1); ;
            } else {
                maternoString = this.nombre.Substring(0, 2);
            }

            this.wrfc = this.paterno.Substring(0, 1) + letra + maternoString;
			this.wrfc = this.wrfc + this.wnumer6;
			//show_it(wrfc)
		}

		private void CalcularHomoclave(String nombreCompleto, String fecha)
		{
			nombreCompleto = nombreCompleto.ToUpper();
			//Guardara el nombre en su correspondiente numérico 
			//agregamos un cero al inicio de la representación númerica del nombre 
			String nombreEnNumero = "0";
			//La suma de la secuencia de números de nombreEnNumero 
			int valorSuma = 0;

			/* region Tablas para calcular la homoclave 
            Estas tablas realmente no se porque son como son 
            solo las copie de lo que encontré en internet */
			Dictionary<string, string> tablaRFC1 = new Dictionary<string, string>();
			tablaRFC1.Add("&", "10");
			tablaRFC1.Add("Ñ", "10");
			tablaRFC1.Add("A", "11");
			tablaRFC1.Add("B", "12");
			tablaRFC1.Add("C", "13");
			tablaRFC1.Add("D", "14");
			tablaRFC1.Add("E", "15");
			tablaRFC1.Add("F", "16");
			tablaRFC1.Add("G", "17");
			tablaRFC1.Add("H", "18");
			tablaRFC1.Add("I", "19");
			tablaRFC1.Add("J", "21");
			tablaRFC1.Add("K", "22");
			tablaRFC1.Add("L", "23");
			tablaRFC1.Add("M", "24");
			tablaRFC1.Add("N", "25");
			tablaRFC1.Add("O", "26");
			tablaRFC1.Add("P", "27");
			tablaRFC1.Add("Q", "28");
			tablaRFC1.Add("R", "29");
			tablaRFC1.Add("S", "32");
			tablaRFC1.Add("T", "33");
			tablaRFC1.Add("U", "34");
			tablaRFC1.Add("V", "35");
			tablaRFC1.Add("W", "36");
			tablaRFC1.Add("X", "37");
			tablaRFC1.Add("Y", "38");
			tablaRFC1.Add("Z", "39");
			tablaRFC1.Add("0", "00");
			tablaRFC1.Add("1", "01");
			tablaRFC1.Add("2", "02");
			tablaRFC1.Add("3", "03");
			tablaRFC1.Add("4", "04");
			tablaRFC1.Add("5", "05");
			tablaRFC1.Add("6", "06");
			tablaRFC1.Add("7", "07");
			tablaRFC1.Add("8", "08");
			tablaRFC1.Add("9", "09");

			Dictionary<int, string> tablaRFC2 = new Dictionary<int, string>();
			tablaRFC2.Add(0, "1");
			tablaRFC2.Add(1, "2");
			tablaRFC2.Add(2, "3");
			tablaRFC2.Add(3, "4");
			tablaRFC2.Add(4, "5");
			tablaRFC2.Add(5, "6");
			tablaRFC2.Add(6, "7");
			tablaRFC2.Add(7, "8");
			tablaRFC2.Add(8, "9");
			tablaRFC2.Add(9, "A");
			tablaRFC2.Add(10, "B");
			tablaRFC2.Add(11, "C");
			tablaRFC2.Add(12, "D");
			tablaRFC2.Add(13, "E");
			tablaRFC2.Add(14, "F");
			tablaRFC2.Add(15, "G");
			tablaRFC2.Add(16, "H");
			tablaRFC2.Add(17, "I");
			tablaRFC2.Add(18, "J");
			tablaRFC2.Add(19, "K");
			tablaRFC2.Add(20, "L");
			tablaRFC2.Add(21, "M");
			tablaRFC2.Add(22, "N");
			tablaRFC2.Add(23, "P");
			tablaRFC2.Add(24, "Q");
			tablaRFC2.Add(25, "R");
			tablaRFC2.Add(26, "S");
			tablaRFC2.Add(27, "T");
			tablaRFC2.Add(28, "U");
			tablaRFC2.Add(29, "V");
			tablaRFC2.Add(30, "W");
			tablaRFC2.Add(31, "X");
			tablaRFC2.Add(32, "Y");
			tablaRFC2.Add(33, "Z");

			Dictionary<string, int> tablaRFC3 = new Dictionary<string, int>();
			tablaRFC3.Add("A", 10);
			tablaRFC3.Add("B", 11);
			tablaRFC3.Add("C", 12);
			tablaRFC3.Add("D", 13);
			tablaRFC3.Add("E", 14);
			tablaRFC3.Add("F", 15);
			tablaRFC3.Add("G", 16);
			tablaRFC3.Add("H", 17);
			tablaRFC3.Add("I", 18);
			tablaRFC3.Add("J", 19);
			tablaRFC3.Add("K", 20);
			tablaRFC3.Add("L", 21);
			tablaRFC3.Add("M", 22);
			tablaRFC3.Add("N", 23);
			tablaRFC3.Add("O", 25);
			tablaRFC3.Add("P", 26);
			tablaRFC3.Add("Q", 27);
			tablaRFC3.Add("R", 28);
			tablaRFC3.Add("S", 29);
			tablaRFC3.Add("T", 30);
			tablaRFC3.Add("U", 31);
			tablaRFC3.Add("V", 32);
			tablaRFC3.Add("W", 33);
			tablaRFC3.Add("X", 34);
			tablaRFC3.Add("Y", 35);
			tablaRFC3.Add("Z", 36);
			tablaRFC3.Add("0", 0);
			tablaRFC3.Add("1", 1);
			tablaRFC3.Add("2", 2);
			tablaRFC3.Add("3", 3);
			tablaRFC3.Add("4", 4);
			tablaRFC3.Add("5", 5);
			tablaRFC3.Add("6", 6);
			tablaRFC3.Add("7", 7);
			tablaRFC3.Add("8", 8);
			tablaRFC3.Add("9", 9);
			tablaRFC3.Add("", 24);
			tablaRFC3.Add(" ", 37);

			String c;

			//Recorremos el nombre y vamos convirtiendo las letras en 
			//su valor numérico 
			int len_nombreCompleto = nombreCompleto.Length;
			for (int x = 0; x < len_nombreCompleto; x++)
			{
				c = nombreCompleto.Substring(x, 1);
				if (tablaRFC1.ContainsKey(c))
					nombreEnNumero = nombreEnNumero + tablaRFC1[c];
				else
					nombreEnNumero = nombreEnNumero + "00";
			}

			//Calculamos la suma de la secuencia de números 
			//calculados anteriormente 
			//la formula es: 
			//( (el caracter actual multiplicado por diez) 
			//mas el valor del caracter siguiente ) 
			//(y lo anterior multiplicado por el valor del caracter siguiente) 

			int n = nombreEnNumero.Length - 1;
			int prod1;
			int prod2;

			for (int i = 0; i < n; i++)
			{
				prod1 = int.Parse(nombreEnNumero.Substring(i, 2));
				prod2 = int.Parse(nombreEnNumero.Substring(i + 1, 1));
				valorSuma += prod1 * prod2;
			}

			//Lo siguiente no se porque se calcula así, es parte del algoritmo. 
			//Los magic numbers que aparecen por ahí deben tener algún origen matemático 
			//relacionado con el algoritmo al igual que el proceso mismo de calcular el 
			//digito verificador. 
			//Por esto no puedo añadir comentarios a lo que sigue, lo hice por acto de fe. 
			decimal div = 0;
			decimal mod = 0;
			div = valorSuma % 1000;
			decimal cociente = div / 34;
			mod = Math.Floor(cociente); //cociente 
			div = div - mod * 34; //residuo 


			String hc = tablaRFC2[(int)mod];
			hc = hc + tablaRFC2[(int)div];

			this.wrfc = this.wrfc + hc;

			//Aqui empieza el calculo del digito verificador basado en lo que tenemos del RFC 
			//En esta parte tampoco conozco el origen matemático del algoritmo como para dar 
			//una explicación del proceso, así que ¡tengamos fe hermanos!. 

			int sumaParcial = 0;
			n = this.wrfc.Length;
			for (int i = 0; i < n; i++)
			{
				c = this.wrfc.Substring(i, 1).ToUpper();
				if (tablaRFC3.ContainsKey(c))
				{
					sumaParcial += (tablaRFC3[c] * (14 - (i + 1)));
				}
			}

			int moduloVerificador = sumaParcial % 11;
			if (moduloVerificador == 0)
				this.wrfc = this.wrfc + "0";
			else
			{
				sumaParcial = 11 - moduloVerificador;
				if (sumaParcial == 10)
					this.wrfc = this.wrfc + "A";
				else
					this.wrfc = this.wrfc + sumaParcial;
			}
		}

		private void _cuarta() { }
		private void _septima() { }
    }
}
